
# Changelog for social-dockbar-hook

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v7.0.0] - 2020-10-01

ported to git and added patch for my account URL pointing to keycloak user acount URL

## [v1.0.0] - 2015-10-06

First release 
